import numpy as np
import matplotlib.pyplot as plt

class DataFileProcess:

	type_of_data = 'for-linear-regression'

	def __init__(self, path_of_data):

		self.path_of_data = path_of_data
		self.number_of_column = 0
		self.number_of_row = 0

		with open(path_of_data, "r", encoding="utf-8") as file:
			data = []
			for line in file:
				if line[0] == "#":
					continue
				data.append([float(i) for i in line.replace('\n', '').split(',')])
				self.number_of_row += 1
			self.number_of_column = len(data[0])

		self.array = np.array(data)

	def PlotWithMatplot(self, pyplot_axis, column1, column2, linestyle=' ', marker='.'):

		if self.number_of_column < column1 + 1 or self.number_of_column < column2 + 1:
			print('Exceeding the number of column in the data!')
			exit()
			
		x = [i[column1] for i in self.array]
		y = [i[column2] for i in self.array]
		pyplot_axis.plot(x, y, linestyle=linestyle, marker=marker, label=self.path_of_data)