import numpy as np
import matplotlib.pyplot as plt
from lib.InputAndOutput import DataFileProcess


data1 = DataFileProcess('linear-regression-data1.txt')
data2 = DataFileProcess('linear-regression-data2.txt')
#print(data1.number_of_row, data1.number_of_column)


fig, ax = plt.subplots()
data1.PlotWithMatplot(ax, 0, 1)
data2.PlotWithMatplot(ax, 0, 1)
ax.legend()
plt.savefig('linear-regression-data.png')
plt.show()


